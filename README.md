# daverona/sphinx

[![pipeline status](https://gitlab.com/toscana/docker/sphinx/badges/master/pipeline.svg)](https://gitlab.com/toscana/docker/sphinx/commits/master)

* GitLab source repository:: [https://gitlab.com/toscana/docker/sphinx](https://gitlab.com/toscana/docker/sphinx)
* Docker Hub repository: [https://hub.docker.com/r/daverona/sphinx](https://hub.docker.com/r/daverona/sphinx)

This is a Docker image for Sphinx on Alpine Linux. This image provides:

* [Sphinx](http://www.sphinx-doc.org/en/master/) 2.0.1

## Installation

Install [Docker](https://hub.docker.com/search/?type=edition&offering=community)
if you don't have one.
Then pull the image from Docker Hub repository:

```bash
docker image pull daverona/sphinx
```

or build the image:

```bash
docker image build \
  --build-arg TIMEZONE=UTC \
  --tag daverona/sphinx \
  .
```

When you build the image, you can specify a time zone of Alpine Linux.
If you don't specify the value of build argument `TIMEZONE`, UTC will be used.

## Quick Start

Run the container:

```bash
docker container run --rm \
  daverona/sphinx \
  sphinx-build --version
```

It will show the version of Sphinx in the container.

## Usage

### Creating a Project

```bash
docker container run --rm \
  --interactive --tty \
  --volume /host/path/to/src:/var/local \
  daverona/sphinx \
  sphinx-quickstart
```

This command asks you about the structure of the documentation project
you are creating and the name, the author, and the initial version of
the project. Then creates the documentation project under /host/path/to/src
directory.

Depending on your answer to "Separate source and build directories" question,
the documentation *source directory* where you keep your reStructuredText or
Markdown files and the documentation *build directory* will differ:

| Separation | Source directory | Build directory |
|---|---|---|
| No | /host/path/to/src | /host/path/to/src/_build |
| Yes | /host/path/to/src/source | /host/path/to/src/build |

To configure your project, please read
[http://www.sphinx-doc.org/en/master/man/sphinx-quickstart.html](http://www.sphinx-doc.org/en/master/man/sphinx-quickstart.html).

### Serving a Project

```bash
docker container run --rm \
  --detach \
  --volume /host/path/to/src:/var/local \
  --publish 8000:8000 \
  --name sphinx \
  daverona/sphinx
```

This command serves your documentation source directory as a website. Sphinx
will automatically translate whatever changes you make to reStructuredText
and/or MarkDown files in real time (well almost), place the result under
the build directory and publish it at
[http://localhost:8000](http://localhost:8000).

### Building a Static Site

```bash
docker container run --rm --tty \
  --volume /host/path/to/src:/var/local \
  daverona/sphinx \
  make html
```

This command generates a static site (e.g. a collection of HTML, JavaScript and
CSS files) from your documentation source directory and place it under a
directory named html in the build directory. You can open index.html under it
with a broswer to verify the output.

### Making a PDF

```bash
docker container run --rm --tty \
  --volume /host/path/to/src:/var/local \
  daverona/sphinx \
  make latexpdf
```

This command generate a PDF file from your documentation source directory and
place it under a directory named latex in the build directory.

To learn more about the output formats, please read
[http://www.sphinx-doc.org/en/master/man/sphinx-build.html](http://www.sphinx-doc.org/en/master/man/sphinx-build.html).

## References

* [http://www.sphinx-doc.org/en/master/](http://www.sphinx-doc.org/en/master/)
* [http://www.sphinx-doc.org/en/master/man/sphinx-quickstart.html](http://www.sphinx-doc.org/en/master/man/sphinx-quickstart.html)
* [http://www.sphinx-doc.org/en/master/man/sphinx-build.html](http://www.sphinx-doc.org/en/master/man/sphinx-build.html)
* [http://www.sphinx-doc.org/en/master/man/sphinx-autogen.html](http://www.sphinx-doc.org/en/master/man/sphinx-autogen.html)
* [http://www.sphinx-doc.org/en/master/man/sphinx-apidoc.html](http://www.sphinx-doc.org/en/master/man/sphinx-apidoc.html)
