#!/bin/sh
set -e

if [ "$#" -eq 0 ]; then
  if [ -d ./_build ]; then
    SOURCE=.
    TARGET=_build
  elif [ -d ./build ]; then
    SOURCE=source
    TARGET=build
  else
    exec sphinx-autobuild --help
  fi
  exec sphinx-autobuild \
    --host 0.0.0.0 \
    --port 8000 \
    --ignore "*~" \
    --ignore "*.swp" \
    --ignore "*.pdf" \
    --ignore "*.log" \
    --ignore "*.out" \
    --ignore "*.toc" \
    --ignore "*.aux" \
    --ignore "*.idx" \
    --ignore "*.ind" \
    --ignore "*.ilg" \
    --ignore "*.tex" \
    $SOURCE \
    $TARGET/html
fi

exec "$@"
