FROM alpine:3.10

# Set the system time zone
# @see https://wiki.alpinelinux.org/wiki/Setting_the_timezone
ARG APP_TIMEZONE=UTC
RUN apk add --no-cache tzdata \
  && cp "/usr/share/zoneinfo/$APP_TIMEZONE" /etc/localtime \
  && echo "$APP_TIMEZONE" > /etc/timezone

# Install Sphinx
ARG SPHINX_VERSION=2.0.1
RUN apk add --no-cache \
    dpkg \
    make \
    python3 \
  && update-alternatives --install /usr/bin/python python /usr/bin/python3 10 \
  && update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10 \
  && pip install --no-cache-dir \
    pandoc \
    recommonmark \
    sphinx==$SPHINX_VERSION \
    sphinx-autobuild \
    sphinx-rtd-theme

# Installing LaTeX
#RUN apk add --no-cache \
#    texlive \
#    # texlive-luatex \
#    # texlive-xetex \
#    texmf-dist \
#    texmf-dist-formatsextra \
#    texmf-dist-latexextra \
#    texmf-dist-pictures \
#    texmf-dist-science

# Additional installation

# Install LaTex texmf-dist-langkorean package for Korean
# RUN apk add --no-cache texmf-dist-langkorean

# Configure entrypoint, ports and working directory
COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
EXPOSE 8000/tcp
WORKDIR /var/local

ENTRYPOINT ["/docker-entrypoint.sh"]
